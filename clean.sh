#! /bin/bash

echo "Welcome to TuxClean, $USER. This script will clean your system cache as well as update repositories."
echo "Please enter your password to run Pacman as admin. None of these actions should break anything."
sudo pacman -Sy --noconfirm
sudo pacman -Sc
sudo pacman -Rns $(pacman -Qtdq) 
echo "If you would like to clear system cache please press Y, if you dont, press N"
sudo du -sh ~/.cache/
echo "This is how large your cache is. Now to clear it"
rm -rf -i ~/.cache/*
echo "Your pacman has now been updated, as well as system and pacman cache. Goodbye"
exit
